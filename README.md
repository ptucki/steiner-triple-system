# Steiner triple system construction algorithm

This project contains an implementation of hill-climbing algorithm to construct [Steiner triple system](https://en.wikipedia.org/wiki/Steiner_system#Steiner_triple_systems) from a set of numbers of supplied size.

## Quickstart

1. Install `python3.6`.
2. `python index.py 301` 
