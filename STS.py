from random import choice, sample
from typing import Dict, List, Set, Optional


class STS:
    # Live pair is a pair of points from V that still can be used (not yet present in any block).
    LivePairs: Dict[int, Set[int]]
    # Live point is a point from V that still can be used (not yet present in maximum number >>(v-1)/2<< of blocks).
    LivePoints: Set[int]
    # Size of currently existing partial STS.
    NumBlocks: int
    # Keep track of other point in a block for each pair.
    Other: Dict[int, Dict[int, Optional[int]]]
    # Supplied set of points to build STS from.
    V: Set[int]
    # The size of supplied set of points to build STS from.
    v: int

    def __init__(self, V: Set[int]):
        self.V = V
        self.v = len(V)
        self.LivePairs = {x: {y for y in self.V if x != y} for x in self.V}
        self.LivePoints = V.copy()
        self.NumBlocks = 0
        self.Other = {x: {y: None for y in self.V if x != y} for x in self.V}
        self.work()

    def work(self) -> None:
        while self.NumBlocks < self.v * (self.v - 1) / 6:
            self.switch()

    def switch(self) -> None:
        x = choice(tuple(self.LivePoints))
        [y, z] = sample(tuple(self.LivePairs[x]), 2)
        if self.Other[y][z] is None:
            self.add_block(x, y, z)
            self.NumBlocks += 1
        else:
            w = self.Other[y][z]
            self.exchange_block(x, y, z, w)

    def add_block(self, x: int, y: int, z: int) -> None:
        self.Other[x][y] = z
        self.Other[y][x] = z
        self.Other[x][z] = y
        self.Other[z][x] = y
        self.Other[y][z] = x
        self.Other[z][y] = x
        self.delete_live_pair(x, y)
        self.delete_live_pair(x, z)
        self.delete_live_pair(y, z)

    def exchange_block(self, x: int, y: int, z: int, w: int) -> None:
        self.Other[x][y] = z
        self.Other[y][x] = z
        self.Other[x][z] = y
        self.Other[z][x] = y
        self.Other[y][z] = x
        self.Other[z][y] = x
        self.Other[w][y] = None
        self.Other[y][w] = None
        self.Other[w][z] = None
        self.Other[z][w] = None
        self.insert_live_pair(w, y)
        self.insert_live_pair(w, z)
        self.delete_live_pair(x, y)
        self.delete_live_pair(x, z)

    def delete_live_pair(self, x: int, y: int) -> None:
        self.LivePairs[x].remove(y)
        self.LivePairs[y].remove(x)
        if len(self.LivePairs[x]) == 0:
            self.LivePoints.remove(x)
        if len(self.LivePairs[y]) == 0:
            self.LivePoints.remove(y)

    def insert_live_pair(self, x: int, y: int) -> None:
        if len(self.LivePairs[x]) == 0:
            self.LivePoints.add(x)
        if len(self.LivePairs[y]) == 0:
            self.LivePoints.add(y)
        self.LivePairs[x].add(y)
        self.LivePairs[y].add(x)

    def construct_blocks(self) -> List[Set[int]]:
        blocks = []
        for x in self.V:
            for y in self.V:
                if x == y:
                    continue
                z = self.Other[x][y]
                if x > y > z:
                    blocks.append({x, y, z})
        return blocks
