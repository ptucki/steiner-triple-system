import sys

from STS import STS

try:
    v = int(sys.argv[1])
except IndexError:
    sys.exit('Required `v` argument must be supplied.')
except ValueError:
    sys.exit('Supplied `v` argument must be numeric.')

if v < 0:
    sys.exit('Supplied `v` argument must be positive.')

if v % 6 not in (1, 3):
    sys.exit('Supplied `v` argument must be congruent to 1 or 3 mod 6.')

V = set(range(0, v))

sts = STS(set(V))
blocks = sts.construct_blocks()

print(str(v * (v - 1) // 6) + ' blocks expected')
print(str(len(blocks)) + ' blocks found')
for block in blocks:
    print(block)
